# Aide et ressources de Fastosh pour Synchrotron SOLEIL

## Résumé

- Analyses données XAFS
- Créé à Synchrotron Soleil

## Sources

- Code source: à voir avec la ligne SAMBA
- Documentation officielle: à voir avec la ligne SAMBA

## Installation

- Systèmes d'exploitation supportés: Windows,  MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: ASCII & HDF
- en sortie: ASCII & HDF
- sur un disque dur,  Ordinateur personnel
